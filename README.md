Ttree Utility
=============

TYPO3 Flow package with general utilities (log backend, localization helper, ...) and custom Doctrine AST for MySQL

Acknowledgments
---------------

Development sponsored by [ttree ltd - neos solution provider](http://ttree.ch).

We try our best to craft this package with a lots of love, we are open to sponsoring, support request, ... just contact us.

License
-------

Licensed under MIT, see [LICENSE](LICENSE)
