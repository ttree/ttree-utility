<?php
namespace Ttree\Utility\Query\AST;

/*
 * This file is part of the Ttree.Utility package.
 *
 * (c) ttree ltd & other contributors - www.ttree.ch
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;
use TYPO3\Flow\Annotations as Flow;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;

class MysqlYearWeek extends FunctionNode
{
    public $firstSimpleArithmeticExpression;
    public $secondSimpleArithmeticExpression = null;

    public function getSql(SqlWalker $sqlWalker)
    {
        $firstExpression = $sqlWalker->walkSimpleArithmeticExpression($this->firstSimpleArithmeticExpression);
        if ($this->secondSimpleArithmeticExpression !== null) {
            $secondExpression = $sqlWalker->walkSimpleArithmeticExpression($this->secondSimpleArithmeticExpression);
            $sql = sprintf('YEARWEEK(%s, %s)', $firstExpression, $secondExpression);
        } else {
            $sql = sprintf('YEARWEEK(%s)', $firstExpression);
        }

        return $sql;
    }

    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);

        $this->firstSimpleArithmeticExpression = $parser->SimpleArithmeticExpression();

        $lexer = $parser->getLexer();
        if ($lexer->isNextToken(Lexer::T_COMMA)) {
            $parser->match(Lexer::T_COMMA);

            $this->secondSimpleArithmeticExpression = $parser->SimpleArithmeticExpression();
        }

        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}
