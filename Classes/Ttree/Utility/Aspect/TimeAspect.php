<?php
namespace Ttree\Utility\Aspect;

/*
 * This file is part of the Ttree.Utility package.
 *
 * (c) ttree ltd & other contributors - www.ttree.ch
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Aop\JoinPointInterface;
use TYPO3\Flow\Log\SystemLoggerInterface;

/**
 * @Flow\Aspect
 */
class TimeAspect
{
    /**
     * @var SystemLoggerInterface
     * @Flow\Inject
     */
    protected $systemLogger;

    /**
     * @param JoinPointInterface $joinPoint
     * @Flow\Around("methodAnnotatedWith(Ttree\Utility\Annotations\Time)")
     * @return mixed
     */
    public function time(JoinPointInterface $joinPoint)
    {
        $start = microtime(true);

        $result = $joinPoint->getAdviceChain()->proceed($joinPoint);

        $time = (microtime(true) - $start) * 1000;

        $this->systemLogger->log('Method \\' . $joinPoint->getClassName().'::'.$joinPoint->getMethodName() . '() took ' . $time .' ms to complete.');

        return $result;
    }
}
