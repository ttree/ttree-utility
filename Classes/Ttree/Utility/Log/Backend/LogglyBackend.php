<?php
namespace Ttree\Utility\Log\Backend;

/*
 * This file is part of the Ttree.Utility package.
 *
 * (c) ttree ltd & other contributors - www.ttree.ch
 *
 * This package is Open Source Software. For the full copyright and license
 * information, please view the LICENSE file which was distributed with this
 * source code.
 */

use TYPO3\Flow\Annotations as Flow;
use TYPO3\Flow\Log\Backend\AbstractBackend;
use TYPO3\Flow\Utility\Now;

class LogglyBackend extends AbstractBackend
{
    /**
     * @var string
     */
    protected $key;

    /**
     * @var array
     */
    protected $severityLabels = array();

    /**
     * Carries out all actions necessary to prepare the logging backend, such as opening
     * the log file or opening a database connection.
     *
     * @return void
     * @api
     */
    public function open()
    {
        $this->severityLabels = array(
            LOG_EMERG   => 'EMERGENCY',
            LOG_ALERT   => 'ALERT    ',
            LOG_CRIT    => 'CRITICAL ',
            LOG_ERR     => 'ERROR    ',
            LOG_WARNING => 'WARNING  ',
            LOG_NOTICE  => 'NOTICE   ',
            LOG_INFO    => 'INFO     ',
            LOG_DEBUG   => 'DEBUG    ',
        );
    }

    /**
     * Appends the given message along with the additional information into the log.
     *
     * @param string $message The message to log
     * @param integer $severity One of the LOG_* constants
     * @param mixed $additionalData A variable containing more information about the event to be logged
     * @param string $packageKey Key of the package triggering the log (determined automatically if not specified)
     * @param string $className Name of the class triggering the log (determined automatically if not specified)
     * @param string $methodName Name of the method triggering the log (determined automatically if not specified)
     * @return void
     * @api
     */
    public function append($message, $severity = LOG_INFO, $additionalData = null, $packageKey = null, $className = null, $methodName = null)
    {
        if ($severity > $this->severityThreshold || empty($this->key)) {
            return;
        }

        $severityLabel = (isset($this->severityLabels[$severity])) ? $this->severityLabels[$severity] : 'UNKNOWN';
        $now = new Now();
        $output = array(
            'eventTime' => $now->format(\DateTime::ISO8601),
            'from' => gethostname(),
            'severity' => trim($severityLabel),
            'packageKey' => $packageKey,
            'message' => $message,
            'className' => $className,
            'methodName' => $methodName
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://logs-01.loggly.com/inputs/'.$this->key.'/tag/flow/');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($output));
        curl_exec($ch);
        curl_close($ch);
    }

    /**
     * Carries out all actions necessary to cleanly close the logging backend, such as
     * closing the log file or disconnecting from a database.
     *
     * @return void
     * @api
     */
    public function close()
    {
    }

    /**
     * @param string $key
     * @return LogglyBackend
     */
    public function setKey($key)
    {
        $this->key = trim($key);
        return $this;
    }
}
